#include "spellcheckworker.h"
#include "singleitem.h"

void SpellCheckItem::submitResults() {
    m_ItemToCheck->setErrors(m_WrongWords);
}

void SpellCheckWorker::runWorkerLoop() {
    for (;;) {
        if (m_Cancel) { break; }

        m_QueueMutex.lock();

        if (m_Queue.isEmpty()) {
            m_WaitAnyItem.wait(&m_QueueMutex);

            // can be cleared by clearCurrectRequests()
            if (m_Queue.isEmpty()) {
                m_QueueMutex.unlock();
                continue;
            }
        }

        SpellCheckItem *item = m_Queue.first();
        m_Queue.removeFirst();

        m_QueueMutex.unlock();

        if (item == NULL) { break; }

        bool canDelete = true;

        try {
            canDelete = processOneItem(item);
        }
        catch (...) {
            qDebug() << "Worker loop: Error while processing item";
        }

        if (canDelete) {
            delete item;
        }
    }
}

bool SpellCheckWorker::processOneItem(SpellCheckItem *item) {
    QString caption = item->m_ItemToCheck->getCaption();
    QStringList parts = caption.split(QChar::Space, QString::SkipEmptyParts);

    int size = parts.size();
    for (int i = 0; i < size; ++i) {
        if (i % 2) {
            item->m_WrongWords.insert(parts[i]);
        }
    }

    item->submitResults();

    return true;
}

