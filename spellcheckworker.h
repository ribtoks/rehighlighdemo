#ifndef SPELLCHECKWORKER_H
#define SPELLCHECKWORKER_H

#include <QWaitCondition>
#include <QMutex>
#include <QVector>
#include <QDebug>
#include <QString>
#include <QSet>

class SingleItem;

class SpellCheckItem {
public:
    SpellCheckItem(SingleItem *item) :
        m_ItemToCheck(item)
    { }

public:
    void submitResults();

public:
    SingleItem *m_ItemToCheck;
    QSet<QString> m_WrongWords;
};

class SpellCheckWorker : public QObject
{
    Q_OBJECT
public:
    SpellCheckWorker():
        m_Cancel(false),
        m_IsRunning(false)
    { }
    virtual ~SpellCheckWorker() { qDeleteAll(m_Queue); }

public:
    void submitItem(SpellCheckItem *item) {
        m_QueueMutex.lock();
        {
            m_Queue.append(item);
        }
        m_QueueMutex.unlock();
        m_WaitAnyItem.wakeOne();
    }

    void submitItems(const QVector<SpellCheckItem*> &items) {
        m_QueueMutex.lock();
        {
            m_Queue << items;
        }
        m_QueueMutex.unlock();
        m_WaitAnyItem.wakeOne();
    }

public slots:
    void process() { runWorkerLoop(); }
    void cancel() { cancelWork(); }

signals:
    void stopped();

private:
    void runWorkerLoop();

    void cancelWork() {
        m_Cancel = true;
        submitItem(NULL);
    }

    bool processOneItem(SpellCheckItem *item);

private:
    QWaitCondition m_WaitAnyItem;
    QMutex m_QueueMutex;
    QVector<SpellCheckItem *> m_Queue;
    volatile bool m_Cancel;
    volatile bool m_IsRunning;
};

#endif // SPELLCHECKWORKER_H
