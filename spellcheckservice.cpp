#include <QThread>

#include "spellcheckservice.h"
#include "spellcheckworker.h"

SpellCheckService::SpellCheckService() {
    m_SpellCheckWorker = new SpellCheckWorker();
}

void SpellCheckService::startChecking() {
    QThread *thread = new QThread();
    m_SpellCheckWorker->moveToThread(thread);

    QObject::connect(thread, SIGNAL(started()), m_SpellCheckWorker, SLOT(process()));
    QObject::connect(m_SpellCheckWorker, SIGNAL(stopped()), thread, SLOT(quit()));

    QObject::connect(m_SpellCheckWorker, SIGNAL(stopped()), m_SpellCheckWorker, SLOT(deleteLater()));
    QObject::connect(thread, SIGNAL(finished()), thread, SLOT(deleteLater()));

    thread->start();
}

void SpellCheckService::submitItems(const QVector<SingleItem *> &items) {
    QVector<SpellCheckItem *> itemsToSubmit;
    itemsToSubmit.reserve(items.length());

    foreach (SingleItem *item, items) {
        itemsToSubmit.append(new SpellCheckItem(item));
    }

    m_SpellCheckWorker->submitItems(itemsToSubmit);
}
