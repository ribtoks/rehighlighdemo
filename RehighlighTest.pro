TEMPLATE = app

QT += qml quick widgets

SOURCES += main.cpp \
    itemsmodel.cpp \
    spellcheckworker.cpp \
    spellcheckservice.cpp \
    spellcheckhighlighter.cpp

RESOURCES += qml.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Default rules for deployment.
include(deployment.pri)

HEADERS += \
    singleitem.h \
    itemsmodel.h \
    spellcheckworker.h \
    spellcheckservice.h \
    spellcheckhighlighter.h
