#include <QApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include "spellcheckservice.h"
#include "itemsmodel.h"

int main(int argc, char *argv[]) {
    QApplication app(argc, argv);

    SpellCheckService service;
    ItemsModel itemsModel(&service);

    QQmlApplicationEngine engine;
    QQmlContext *rootContext = engine.rootContext();
    rootContext->setContextProperty("itemsModel", &itemsModel);

    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));

    service.startChecking();

    return app.exec();
}
