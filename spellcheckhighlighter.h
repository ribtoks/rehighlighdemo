#ifndef SPELLCHECKHIGHLIGHTER_H
#define SPELLCHECKHIGHLIGHTER_H

#include <QtGui>

class SingleItem;

class SpellCheckHighlighter : public QSyntaxHighlighter
{
    Q_OBJECT
public:
    SpellCheckHighlighter(QTextDocument *document, SingleItem *item);

protected:
    virtual void highlightBlock(const QString &text);

private:
    SingleItem *m_ItemToCheck;
};

#endif // SPELLCHECKHIGHLIGHTER_H
