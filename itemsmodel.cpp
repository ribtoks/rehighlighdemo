#include "itemsmodel.h"
#include "singleitem.h"
#include "spellcheckservice.h"
#include <QQuickTextDocument>
#include "spellcheckhighlighter.h"
#include <QDebug>

ItemsModel::ItemsModel(SpellCheckService *spellCheckService) {
    m_SpellCheckService = spellCheckService;

    m_ItemsList.append(new SingleItem("My first random text"));
    m_ItemsList.append(new SingleItem("My second random text adsfdasf "));
    m_ItemsList.append(new SingleItem("My third random text atqewqr"));
}

ItemsModel::~ItemsModel() {
    qDeleteAll(m_ItemsList);
}

int ItemsModel::rowCount(const QModelIndex &parent) const {
    Q_UNUSED(parent);
    return m_ItemsList.length();
}

QVariant ItemsModel::data(const QModelIndex &index, int role) const {
    int row = index.row();
    if (row < 0 || row >= m_ItemsList.length()) { return QVariant(); }

    if (role == Qt::DisplayRole) {
        return m_ItemsList.at(row)->getCaption();
    } else {
        return QVariant();
    }
}

void ItemsModel::submitToSpellCheck() const {
    m_SpellCheckService->submitItems(m_ItemsList);
}

void ItemsModel::initHighlighter(int index, QQuickTextDocument *document) {
    // Signal mapper could be avoided if lamda slot are available (Qt5 and C++11)
    QSignalMapper* signalMapper = new QSignalMapper(this);

    if (0 <= index && index < m_ItemsList.length()) {
        SingleItem *item = m_ItemsList.at(index);
        SpellCheckHighlighter *highlighter = new SpellCheckHighlighter(document->textDocument(), item);
        QObject::connect(item, SIGNAL(spellCheckResultsReady()),
                         highlighter, SLOT(rehighlight()));

        // TODO: Don't connect this slot for Qt 5.5+ to avoid performance overhead
        QObject::connect(item, SIGNAL(spellCheckResultsReady()),
                         signalMapper, SLOT(map()));
        signalMapper->setMapping(item, index);
    }

    connect(signalMapper, SIGNAL(mapped(int)),
            this, SIGNAL(updateTextEdit(int)));
}



